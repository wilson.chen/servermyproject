const https = require('https');
const URL = require('url');
const jsSHA = require('jssha');
const appid = 'wx931ca524c188544d';
const secret = '89ce769d9fbc228429e2d488293e3d84';

const tokenUrl =
  `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${appid}&secret=${secret}`;

function getReadyWechat(url) {
  const promise = new Promise((resolve, reject) => {
    https.get(tokenUrl, res => {
      res.on('data', function(chunk) {
        const _res = JSON.parse(chunk);

        const ticktedUrl = `https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=${
          _res.access_token
        }&type=jsapi`;
        https.get(ticktedUrl, function(ticketResponse) {
          ticketResponse.on('data', function(ticketChunk) {
            const ticketRes = JSON.parse(ticketChunk);
            console.log('ticket: ' + ticketRes.ticket);
            resolve(createSignature(ticketRes.ticket, url));
          }); // 这个异步回调里可以获取ticket
        });
      });
    });
  });

  return promise;
}

function getTicktet(token, url, callback) {
  const ticktedUrl = `https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=${token}&type=jsapi`;
  https.get(ticktedUrl, function(res) {
    res.on('data', function(chunk) {
      const _res = JSON.parse(chunk);
      createSignature(_res.ticket, url, callback);
    }); // 这个异步回调里可以获取ticket
  });
}

function createSignature(ticket, url) {
  const nonceStr = Math.random()
    .toString(36)
    .substr(2, 15);
  // timestamp
  const timestamp = parseInt(new Date().getTime() / 1000) + '';
  var str =
    'jsapi_ticket=' +
    ticket +
    '&noncestr=' +
    nonceStr +
    '&timestamp=' +
    timestamp +
    '&url=' +
    url;
  shaObj = new jsSHA('SHA-1', 'TEXT');
  shaObj.update(str);
  const signature = shaObj.getHash('HEX');
  result = {
    appid,
    timestamp,
    nonceStr,
    signature
  };
  // callback(result);
  return result;
}

function test() {



  const jsapi_ticket = 'kgt8ON7yVITDhtdwci0qeeF4h5kXYfSaLHw0ME17eyL9L2vXnH3HDyTE1-0YAb2ekm6m3YI5FeIH-3BwO0f2oA';
  const timestamp = '1541666504';
  const noncestr = 'a5j5cnvmmu5';
  const url = 'http://wilson.51bbu.com/forum/';
  var str =
    'jsapi_ticket=' +
    jsapi_ticket +
    '&noncestr=' +
    noncestr +
    '&timestamp=' +
    timestamp +
    '&url=' +
    url;
  console.log('str:', str);
  shaObj = new jsSHA('SHA-1', 'TEXT');
  shaObj.update(str);
  const signature = shaObj.getHash('HEX');
  console.log('signature:', signature);
}

module.exports = getReadyWechat;
