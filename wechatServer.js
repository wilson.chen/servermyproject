assert = require('assert');

var cors = require('koa2-cors');
const Koa = require('koa');
const app = new Koa();
const route = require('koa-route');
const  bodyParser = require('koa-bodyparser');

const getReadyWechat = require('./weixin')

app.use(cors());
app.use(bodyParser());

const getWechatInfo = async ctx => {
  // ctx.response.type = 'html';
  console.log(ctx.request.body);
  const { url } = ctx.request.body;
  console.log(5577,url)
  const result = await getReadyWechat(url);
  console.log('result:',result)

  ctx.response.body = result;;
};

function startServer() {
  const main = ctx => {
    ctx.response.body = 'wechat server';
  };

  app.use(route.get('/', main));

  app.use(route.post('/getWechatInfo', getWechatInfo));

  app.listen(3001);
  console.log('已经运行wechat server，端口为3001');
}

startServer();
