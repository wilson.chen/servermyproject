const db = require('monk')('mongodb://192.168.0.104:27017/test');
assert = require('assert');

var cors = require('koa2-cors');
const Koa = require('koa');
const app = new Koa();
const route = require('koa-route');
var bodyParser = require('koa-bodyparser');

// app.use(cors());
app.use(bodyParser());

app.use(cors({
  origin: function (ctx) {
      // if (ctx.url === '/test') {
      //     return "*"; // 允许来自所有域名请求
      // }
      // return 'http://localhost:1081'; // 这样就能只允许 http://localhost:8080 这个域名的请求了
      return '*';
  },
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
  maxAge: 5,
  credentials: true,
  allowMethods: ['GET', 'POST', 'DELETE'],
  allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}));


const getDocuments = async ctx => {
  // ctx.response.type = 'html';
  const res = await db.get('documents').find();
  console.log(res);
  ctx.response.body = res;
};

const getStudents = async ctx => {
  // ctx.response.type = 'html';
  ctx.response.type = 'application/json';
  const res = await db.get('students').find();
  console.log(res);
  ctx.body = res;
};

const addStudent = async ctx => {
  // ctx.response.type = 'html';
  console.log(ctx.request.body);
  const { name, age, gender } = ctx.request.body;

  await db.get('students').insert({
    name,
    age,
    gender
  });

  ctx.response.body = {result: 'SUCCESS'};
};
const removeStudent = async ctx => {
  // ctx.response.type = 'html';
  console.log(ctx.request.body);
  const { _id } = ctx.request.body;

  await db.get('students').remove({
    _id
  });

  ctx.response.body = {result: 'SUCCESS'};
};

function startServer(db) {
  const main = ctx => {
    ctx.response.body = 'Hello World';
  };

  app.use(route.get('/', main));
  app.use(route.get('/getDocuments', getDocuments));
  app.use(route.get('/getStudents', getStudents));
  app.use(route.post('/addStudent', addStudent));
  app.use(route.post('/removeStudent', removeStudent));

  app.listen(3001);
  console.log('已经运行app，端口为3001');
}

startServer(db);
