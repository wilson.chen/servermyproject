const cors = require('koa2-cors');
const Koa = require('koa');
const app = new Koa();
const route = require('koa-route');
const bodyParser = require('koa-bodyparser');
const USER = {};

function socketServer() {
  app.use(cors());

  const server = require('http').createServer(app.callback());
  const io = require('socket.io')(server);
  io.set('transports', ['websocket']);

  io.on('connection', client => {
    console.log('connection success!');
    client.on('addUser', (data) => {
      USER[data._id] = {
        socketId:client.id,
        ...data
      };
      client.user = data;
      console.log('22', USER)
    });
    client.on('join', data => {
      const { user, room } = data;
      client.user = user;
      console.log('join:', room);
      client.room = room;
      client.join(room);
    });
    client.on('disconnect', () => {
      console.log('disconnect:', new Date().getTime());
    });
    client.on('message', function(data) {
      console.log('message: ' + data.msg);
      console.log(USER);
      io.to(USER[data.userId].socketId).emit('message', {
        user: client.user,
        data: data.msg
      });
    });
  });
  server.listen(3002, () => {
    console.log('socket server running at 3002');
  });

  return {
    io,
    USER
  };
}

module.exports = socketServer;
