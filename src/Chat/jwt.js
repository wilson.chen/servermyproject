const jwt = require('jsonwebtoken');
const user = {
  name: 'ggg'
}

const token = jwt.sign(user, 'app.get(superSecret)', {
  'expiresIn': '1h' // 设置过期时间
});

console.log(token)

jwt.verify(token, 'app.get(superSecret)', function(err, decoded) {
  if (err) {
    console.log('wrong')
  } else {
    // 如果没问题就把解码后的信息保存到请求中，供后面的路由使用
    console.log('right')
  }
});

var decoded = jwt.decode(token, {complete: true});
console.log(decoded.header);
console.log(decoded.payload)