const _ = require('lodash');
const makeAddFriendRequest = function(payload) {
  return {
    payload:_.omit(payload, 'pushInfo'),
    type: 'ADD_FRIEND',
    id: Date.now()
  }
}

module.exports = {
  makeAddFriendRequest
}