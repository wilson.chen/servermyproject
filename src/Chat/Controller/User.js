//@flow
const config = require('../../../Config');
const db = require('monk')(config.dbEvn);
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const md5 = require('blueimp-md5');
const pushService = require('../Service/push');

const localKey = config.jwtLocalkey;

const login = async ctx => {
  // ctx.response.type = 'html';
  ctx.response.type = 'application/json';
  const { name, email, password } = ctx.request.body;

  const res = await db.get('user').findOne({
    name,
    password: md5(password)
  });
  const userInfo = _.omit(res, 'password');

  const token = jwt.sign(userInfo, localKey, {
    expiresIn: '1h' // 设置过期时间
  });
  if (res) {
    ctx.response.body = {
      user: userInfo,
      token,
      result: 'SUCCESS'
    };
  } else {
    ctx.response.body = {
      result: 'INVALID USER'
    };
  }
};

const register = async ctx => {
  // ctx.response.type = 'html';
  const { name, email, password } = ctx.request.body;

  const res = await db.get('user').insert({
    name,
    email,
    password: md5(password)
  });

  ctx.response.body = {
    user: _.omit(res, 'password'),
    result: 'SUCCESS'
  };
};

const getUserFriends = async ctx => {
  ctx.response.type = 'application/json';

  const token = ctx.request.header.token;
  const res = jwt.decode(token, { complete: true });
  const { _id } = res.payload;
  const targetUser = await db.get('user').findOne({
    _id
  });
  const friends = await db.get('user').find({
    _id: {
      $in: targetUser.friends
    }
  });
  ctx.response.body = {
    result: 'SUCCESS',
    friends
  };
};

const logout = async ctx => {
  // ctx.response.type = 'html';

  ctx.response.body = res;
};
const searchUser = async ctx => {
  // ctx.response.type = 'html';

  const { email } = ctx.request.body;
  const user = await db.get('user').findOne({
    email
  });
  ctx.response.body = {
    result: 'SUCCESS',
    user
  };
};
const sendAddUserRequest = async ctx => {
  const { _id } = ctx.request.body;
  const res = await db.get('user').findOne({
    _id: ctx.userData._id
  });
  const targetUser = await db.get('user').findOne({
    _id
  });
  const pushTarget = pushService.makeAddFriendRequest(res);
  let pushInfo = targetUser.pushInfo;
  if (pushInfo) {
    pushInfo.push(pushTarget);
  } else {
    pushInfo = [pushTarget];
  }
  db.get('user').findOneAndUpdate(
    { _id },
    {
      $set: {
        pushInfo
      }
    }
  );
  ctx.response.body = {
    result: 'SUCCESS'
  };
  console.log(11,socketApp.USER)

  global.socketApp.io.to(socketApp.USER[_id]).emit('addFriend', pushTarget);
};
const acceptAddUserRequest = async ctx => {
  const { pushId } = ctx.request.body;
  const self = await db.get('user').findOne({
    _id: ctx.userData._id
  });
  let index = _.findIndex(self.pushInfo, p => p.id === pushId);
  if (index !== -1) {
    const friend = self.pushInfo[index].payload;
    if (!_.includes(self.friends, friend._id)) {
      self.friends.push(friend._id);
    }

    db.get('user').findOneAndUpdate(
      { _id: ctx.userData._id },
      {
        $set: {
          pushInfo: self.pushInfo.splice(index, 1),
          friends: self.friends
        }
      }
    );
  }
  ctx.response.body = {
    result: 'SUCCESS'
  };
};

const checkToken = async function(ctx, next) {
  const req = ctx.request;
  const res = ctx.response;
  if (req.url != '/login' && req.url != '/register') {
    let token = req.header.token;
    if (!token) {
      res.body = {
        result: 'LOGIN EXPIRED'
      };
    } else {
      const res = await jwt.verify(token, localKey);

      // 如果考验通过就next，否则就返回登陆信息不正确
      if (!res) {
        // res.body = {
        //   result: 'LOGIN EXPIRED'
        // };
        return res.json({ success: false, message: 'token信息错误.' });
      } else {
        const data = jwt.decode(token, { complete: true });
        ctx.userData = data.payload;
        await next();
      }
    }
  } else {
    await next();
  }
};

const User = {
  register,
  login,
  logout,
  checkToken,
  getUserFriends,
  searchUser,
  acceptAddUserRequest,
  sendAddUserRequest
};

module.exports = User;
