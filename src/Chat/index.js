// const localhost = 'http://192.168.2.197:27017/test';
// const origin = 'http://140.143.224.99:27017/test';
// const db = require('monk')(localhost);
assert = require('assert');

var cors = require('koa2-cors');
const Koa = require('koa');
const app = new Koa();
const route = require('koa-route');
var bodyParser = require('koa-bodyparser');

const UserController = require('./Controller/User');
const socketServer = require('./socket.js');

app.use(cors({
  origin: function (ctx) {
      // if (ctx.url === '/test') {
          // return "*"; // 允许来自所有域名请求
      // }
      // return 'http://localho st:1081'; // 这样就能只允许 http://localhost:8080 这个域名的请求了
      // return "*";
      return "*"; // 允许来自所有域名请求
  },
  exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
  maxAge: 5,
  credentials: true,
  allowMethods: ['GET', 'POST', 'DELETE'],
  allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'token'],
}));
app.use(bodyParser());

function startServer() {
  global.socketApp = socketServer();
  const main = ctx => {
    ctx.response.body = 'welcome to chat';
  };
  app.use(UserController.checkToken);

  app.use(route.get('/', main));
  app.use(route.post('/register', UserController.register));
  app.use(route.post('/login', UserController.login));
  app.use(route.post('/logout', UserController.logout));
  app.use(route.get('/getUserFriends', UserController.getUserFriends));
  app.use(route.post('/searchUser', UserController.searchUser));
  app.use(route.post('/acceptAddUserRequest', UserController.acceptAddUserRequest));
  app.use(route.post('/sendAddUserRequest', UserController.sendAddUserRequest));

  app.listen(3004);
  console.log('chat server runing at 3004');
}

startServer();
